#!/bin/bash

rm -rf build
git clone git@bitbucket.org:xarthisius/nds-django.git build
pushd build &> /dev/null
git submodule init
git submodule update
pushd ythub_workers &> /dev/null
git pull origin master
popd &> /dev/null
popd &> /dev/null

ln -s Dockerfile.base Dockerfile
docker build -t xarthisius/nds_rest_base:latest .
unlink Dockerfile

ln -s Dockerfile.api Dockerfile
docker build -t xarthisius/nds_rest_api:latest .
unlink Dockerfile

ln -s Dockerfile.worker Dockerfile
docker build -t xarthisius/nds_rest_worker:latest .
unlink Dockerfile
