__author__ = 'keyz'
from rest_framework import serializers
from django.contrib.auth.models import User

from docker_rest.models import \
    Job, JobMetric, JobState, JobStdOut, JobChange, \
    JobFilePath, IrodsList, DockerImagesList


class UserSerializer(serializers.ModelSerializer):
    jobs = serializers.PrimaryKeyRelatedField(many=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'jobs')


class JobSerializer(serializers.ModelSerializer):
    owner = serializers.Field(source='owner.username')

    class Meta:
        model = Job
        fields = ('id', 'scriptname', 'script', 'image_name', 'image_tag',
                  'worker', 'create_task_id', 'create_task_state',
                  'container_id', 'owner')


class IrodsListSerializer(serializers.ModelSerializer):

    class Meta:
        model = IrodsList
        fields = ('id', 'created', 'data')


class DockerImagesListSerializer(serializers.ModelSerializer):

    class Meta:
        model = DockerImagesList
        fields = ('id', 'created', 'data')


class JobMetricSerializer(serializers.ModelSerializer):

    class Meta:
        model = JobMetric
        fields = ('id', 'created', 'data', 'job')


class JobStateSerializer(serializers.ModelSerializer):

    class Meta:
        model = JobState
        fields = ('id', 'created', 'data', 'job')


class JobStdOutSerializer(serializers.ModelSerializer):

    class Meta:
        model = JobStdOut
        fields = ('id', 'created', 'data', 'job')


class JobChangeSerializer(serializers.ModelSerializer):

    class Meta:
        model = JobChange
        fields = ('id', 'created', 'data', 'job')


class JobFilePathSerializer(serializers.ModelSerializer):

    class Meta:
        model = JobFilePath
        fields = ('id', 'created', 'data', 'job', 'path')
