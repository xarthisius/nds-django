__author__ = 'keyz'
from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from views import \
    UploadJob, StatusJob, StdOutJob, MetricsJob, DiffJob, \
    ListDataJob, DownloadJob, ListImages

urlpatterns = [
    url(r'^upload$', UploadJob.as_view()),
    url(r'^status$', StatusJob.as_view()),
    url(r'^stdout$', StdOutJob.as_view()),
    url(r'^metric$', MetricsJob.as_view()),
    url(r'^diff$', DiffJob.as_view()),
    url(r'^list_data$', ListDataJob.as_view()),
    url(r'^download$', DownloadJob.as_view()),
    url(r'^list_images$', ListImages.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)

#  r"/%s/register/?" post register_file_irods,
#  r"/%s/info/?" , get_tasks get_container_info,
#  r"/%s/list_containers/?" get list_containers_docker,
#  r"/%s/stop/?" , get stop_container_docker],
#  r"/%s/list_images/?" , get get_docker_image_list,
