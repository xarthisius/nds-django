# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('scriptname', models.CharField(max_length=256)),
                ('script', models.CharField(max_length=1048576)),
                ('image_name', models.CharField(max_length=256)),
                ('image_tag', models.CharField(max_length=256, blank=True)),
                ('worker', models.CharField(max_length=64, blank=True)),
                ('create_task_id', models.CharField(max_length=128, blank=True)),
                ('create_task_state', models.CharField(max_length=16, null=True, blank=True)),
                ('container_id', models.CharField(max_length=128, null=True, blank=True)),
                ('state', models.CharField(max_length=16, null=True, blank=True)),
                ('owner', models.ForeignKey(related_name='jobs', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('created',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='JobMetric',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('metric_id', models.CharField(max_length=128, blank=True)),
                ('data', models.CharField(max_length=1048576, null=True, blank=True)),
                ('state', models.CharField(max_length=16, null=True, blank=True)),
                ('job', models.ForeignKey(related_name='metrics', to='docker_rest.Job')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='JobState',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('state_id', models.CharField(max_length=128, blank=True)),
                ('data', models.CharField(max_length=1048576, null=True, blank=True)),
                ('state', models.CharField(max_length=16, null=True, blank=True)),
                ('job', models.ForeignKey(related_name='states', to='docker_rest.Job')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
