# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('docker_rest', '0006_irodslist_jobfilepath'),
    ]

    operations = [
        migrations.CreateModel(
            name='DockerImagesList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('data', models.CharField(max_length=1048576, null=True, blank=True)),
                ('task_id', models.CharField(default=b'', max_length=128, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='jobfilepath',
            name='path',
            field=models.CharField(max_length=1024, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='job',
            name='scriptname',
            field=models.CharField(default=b'script.py', max_length=256),
            preserve_default=True,
        ),
    ]
